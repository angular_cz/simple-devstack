# Simple Devstack #

## Co si nainstalovat
- [Java Development Kit (JDK)](http://www.oracle.com/technetwork/java/javase/downloads/index.html )
- [git](http://git-scm.com/downloads)
- aktuální verzi [nodeJS](https://nodejs.org/en/)

## Instalace

Stáhněte tento balíček kamkoli k sobě. 

```
git clone https://bitbucket.org/angular_cz/simple-devstack

 npm install

 npm start
```

Nyní, když otevřete prohlížeč na adrese [http://localhost:8000/](http://localhost:8000/) uvidíte běžící aplikaci.

Co to umí:
```
 npm start        # vývojový režim, spustí aplikaci lokálně, dělá livereload
 
 npm test         # spuštění unit testů (karma)
 
 npm run test-watch   # opakovené spouštění unit testů při změně souborů 

 npm run protractor   # E2E test
 
 npm build            # zbuilduje celou aplikaci do složky dist
```
