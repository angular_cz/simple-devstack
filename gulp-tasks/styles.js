var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

var errorHandlers = require('./_errorHandlers');

exports.sass = function() {
  return gulp.src('src/sass/styles.scss')
    .pipe(plugins.sass().on('error', plugins.sass.logError))
    .pipe(gulp.dest('dependencies/'));
};

exports.cssmin = function() {
  return gulp.src('src/sass/styles.scss')
    .pipe(plugins.sass().on('error', plugins.sass.logError))
    .pipe(plugins.cssmin())
    .pipe(gulp.dest('dist/'));
};

