(function() {
  'use strict';

  angular.module('cz.angular.simpleDevstack', [
    'angularStats',
    'ngMaterial'
  ])
    .controller('BaseController', function() {
      this.ngVersion = angular.version;
    });

})();
